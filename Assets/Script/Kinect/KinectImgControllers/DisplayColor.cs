using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Renderer))]
public class DisplayColor : MonoBehaviour {
	
	public DeviceOrEmulator devOrEmu;
	private Kinect.KinectInterface kinect;
    public int FrameCountDelay = 2;
	private Texture2D tex;
    Queue<Color32[]> queue;
	// Use this for initialization
	void Start () {
        queue = new Queue<Color32[]>();
		kinect = devOrEmu.getKinect();
        tex = new Texture2D(640, 480, TextureFormat.ARGB32, false);
        //tex = new Texture2D(320, 240, TextureFormat.ARGB32, false);
		GetComponent<Renderer>().material.mainTexture = tex;
       // StartCoroutine(Refresh());
	}

    void Update () {
		if (kinect.pollColor())
		{
            Color32[] frame = (Color32[])kinect.getColor().Clone();
            queue.Enqueue(frame);
            if (queue.Count >= FrameCountDelay)
            {
                    Color32[] delayedFrame = queue.Dequeue();
                    tex.SetPixels32(delayedFrame);
                    tex.Apply(false);
            }
		}
	}

 	private Color32[] mirror(Color32[] src, int width, int height)
	{
		int newWidth = width;
		int newHeight = height;
		Color32[] dst = new Color32[newWidth * newHeight];
		for(int yy = 0; yy < newHeight; yy++)
		{
			for(int xx = 0; xx < newWidth; xx++)
			{
                dst[xx + yy * newWidth] = src[(newWidth - xx - 1) + yy * newWidth];
			}
		}
		return dst;
	}
	

}
