﻿using UnityEngine;
using System.Collections;
using Kinect;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using UnityEngine.UI;

public class PointManPlayer : MonoBehaviour {
    float playBackSpeed = 0.0333f;
    float timer = 0f;
    bool isPlaying;
    RecordedPointMan rpm;
    private NuiSkeletonFrame[] skeletonFrame;
    public KinectPointController kinectPointController;
    Vector3 offset = Vector3.zero;
    Vector3 startPosition;
    int currFrame = 0;
    public bool useOffset = true;
    public VideoCutSlider videoCutSlider;
    KinectPointController kpc;

    public float playingStartTimer = -1;
    public Text playingStartTimerText;
    bool offsetUsed;

    public void Load(string name)
    {
        skeletonFrame = null;
        this.SetCurrFrame(0);
        FileStream input = new FileStream(Application.dataPath + "/../Records/" + name, FileMode.Open);
        BinaryFormatter bf = new BinaryFormatter();
        SerialSkeletonFrame[] serialSkeleton = (SerialSkeletonFrame[])bf.Deserialize(input);
        skeletonFrame = new NuiSkeletonFrame[serialSkeleton.Length];
        for (int ii = 0; ii < serialSkeleton.Length; ii++)
        {
            try
            {
                skeletonFrame[ii] = serialSkeleton[ii].deserialize();
            }
            catch (NullReferenceException e)
            {
                break;
            }
        }
        rpm.SetPlayer(-1);
        offsetUsed = false;
        input.Close();
        timer = 0f;
    }

    public void StartCountDown(String name_)
    {
        playingStartTimer = 10f;
        playingStartTimerText.text = ((int)(playingStartTimer) + 1).ToString();
        Load(name_);
        isPlaying = false;
        //skeletonFrames = null;
    }

    public Toggle playstoptoggle;
    public void Start()
    {
        startPosition = transform.position;
        rpm = this.GetComponent<RecordedPointMan>();
        kpc = GameObject.FindObjectOfType<KinectPointController>();
    }

    public void StartPlaying()
    {
        isPlaying = true;
        if (!useOffset)
        {
            currFrame = videoCutSlider.start;
            rpm.offset = Vector3.zero;
        }
        else
            currFrame = 0;
    }

    public void StopPlaying()
    {
        isPlaying = false;
        rpm.SetPlayer(-1);
        currFrame = 0;
        playingStartTimerText.text = "";
        playingStartTimer = -1f;
    }

    public void StopAndRemoveClip()
    {
        isPlaying = false;
        currFrame = 0;
        skeletonFrame = null;
        Debug.Log("stop");
        playingStartTimerText.text = "";
        playingStartTimer = -1f;
    }

    public void UseOffset()
    {
        Vector3 pos = kinectPointController.LegsPosition;
        Vector3[] bones = rpm.GetBones(skeletonFrame[currFrame]);
        Vector3 legsPosition;
        if (bones != null)
            legsPosition = (bones[(int)Kinect.NuiSkeletonPositionIndex.FootLeft] + bones[(int)Kinect.NuiSkeletonPositionIndex.FootRight]) / 2f;
        else
            legsPosition = pos;
        if (kinectPointController.player != -1)
            offset = (legsPosition - pos);
        else offset = Vector3.zero;
        //offset.z = -offset.z;
        rpm.offset = offset;
    }

	void Update () {
        if (playingStartTimer <= 0)
        {
            playingStartTimerText.text = "";
            if (skeletonFrame != null && offsetUsed == true)
            {
                isPlaying = true;
                offsetUsed = false;
            }
        }
        else
        {
            playingStartTimer -= Time.deltaTime;
            playingStartTimerText.text = ((int)(playingStartTimer) + 1).ToString();
            UseOffset();
            offsetUsed = true;
        }

        if (!isPlaying && skeletonFrame == null){
            NuiSkeletonFrame nullFrame =new NuiSkeletonFrame();
            nullFrame.SkeletonData = null;
            rpm.SetFrame(nullFrame);
        }

        if (isPlaying) {
            timer += Time.deltaTime;
            int temp = currFrame;
            float dt = (skeletonFrame[currFrame+1].liTimeStamp - skeletonFrame[currFrame].liTimeStamp) / 1000f;
            while (timer > dt)
            {
                if (timer > dt)
                {
                    currFrame++;
                    if (useOffset)
                    {
                        currFrame = currFrame % (skeletonFrame.Length - 1);
                    }
                    else {
                        if (currFrame > videoCutSlider.end-2)
                            currFrame = videoCutSlider.start;
                    }
                    timer -= dt;
                }
                dt = (skeletonFrame[currFrame+1].liTimeStamp - skeletonFrame[currFrame].liTimeStamp) / 1000f;
            }

            if (temp != currFrame) {
                if (videoCutSlider != null)
                    videoCutSlider.SetCurrentWihoutEvent(currFrame);
                rpm.SetFrame(skeletonFrame[currFrame]);
            }
        }
	}

    public void SetClip(RecordingController recordingController)
    {
        skeletonFrame = recordingController.GetRecordedFrames();
    }

    public void SetCurrFrame(int num)
    {
        this.currFrame = num;
        timer = 0;
    }

    public void SetCurrFrame(VideoCutSlider vcs)
    {
        this.SetCurrFrame(vcs.current);
    }

    public void UseOffset(bool flag)
    {
        useOffset = flag;
    }

    public void StopPlayButton(Toggle toggle)
    {
        if (toggle.isOn)
            StartPlaying();
        else
            StopPlaying();
    }

    public void ChangeToggleToStop()
    {
        playstoptoggle.isOn = true;
    }

    public void ActivateStopPlayButton()
    {
        if (playstoptoggle.gameObject.activeSelf == false)
            playstoptoggle.gameObject.SetActive(true);
    }
}
