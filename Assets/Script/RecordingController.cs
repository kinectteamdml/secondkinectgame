﻿using UnityEngine;
using System.Collections;
using Kinect;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class RecordingController : MonoBehaviour {
    private static RecordingController instance_;
    public static RecordingController Instance
    {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<RecordingController>();
            return instance_;
        }
    }
    public DeviceOrEmulator devOrEmu;
    public ArrayList recordedClip = new ArrayList();
    KinectInterface kinect;
    bool recording = false;
    public VideoCutSlider vcs;
    public RecordedPointMan rpm;
    bool play = false;
    public void Start()
    {
       
        kinect = devOrEmu.getKinect();
        
    }
    public GameObject quitDialog;

	void Update () {
        if (Input.GetKey(KeyCode.Escape))
            quitDialog.SetActive(true);

        if (recording)
        {
            if (kinect.pollSkeleton())
            {
                recordedClip.Add(kinect.getSkeleton());
                vcs.SetRange(0, recordedClip.Count-1);
            }
           
        }
	}

    public void Quit()
    {
        Application.Quit();
    }

    public void SetFrame()
    {
        rpm.SetFrame((int)vcs.current);
    }
    public void StartRecording()
    {
        recordedClip = new ArrayList();
        recording = true;
        rpm.player = -1;
    }

    public void StopRecording()
    {
        recording = false;
    }

    public NuiSkeletonFrame[] GetRecordedFrames()
    {
        NuiSkeletonFrame[] data = new NuiSkeletonFrame[recordedClip.Count];
        for (int i = 0; i < data.Length; i++)
            data[i] = (NuiSkeletonFrame)recordedClip[i];
        
        return data;
    }

    public void SaveRecord(string name)
    {
        string filePath = Application.dataPath + "/../Records/" + name;
        FileStream output = new FileStream(filePath, FileMode.Create);
        BinaryFormatter bf = new BinaryFormatter();
        SerialSkeletonFrame[] data = new SerialSkeletonFrame[vcs.end - vcs.start];
        for (int ii = (int)vcs.start; ii < (int)vcs.end; ii++)
            data[ii - vcs.start] = new SerialSkeletonFrame((NuiSkeletonFrame)recordedClip[ii]);
        bf.Serialize(output, data);
        output.Close();
        Debug.Log("Saved");
    }

    public void SaveRecord(Text t)
    {
        SaveRecord(t.text);
    }
}
