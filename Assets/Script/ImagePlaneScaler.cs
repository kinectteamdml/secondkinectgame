﻿using UnityEngine;
using System.Collections;

public class ImagePlaneScaler : MonoBehaviour {
    public float distance = 4f;
    
	void Start () {
        Camera cam = Camera.main;
        float height = 2.0f * Mathf.Tan(0.5f * cam.fieldOfView * Mathf.Deg2Rad) * distance;
        float width = height * Screen.width / Screen.height;
        float scaleX = width;
        float scaleZ = scaleX * transform.localScale.z/transform.localScale.x;
        this.transform.localScale = new Vector3(-scaleX, 1f, -scaleZ);
	}
}
